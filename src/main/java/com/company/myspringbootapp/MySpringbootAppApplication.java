package com.company.myspringbootapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySpringbootAppApplication {

	public static void main(String[] args) {
		System.out.println("Starting the Springboot Application");
		SpringApplication.run(MySpringbootAppApplication.class, args);
	}

}
